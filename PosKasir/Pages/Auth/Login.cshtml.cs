using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir.Pages.Auth
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly AlatTulisService _alatTulisMan;


        [BindProperty]
        public LoginFormModel Form { get; set; }
        public string RoleDb { get; set; }
        [TempData]
        public string SuccessMessage { get; set; }

        public LoginModel(AlatTulisService alatTulisService)
        {
            this._alatTulisMan = alatTulisService;
        }
        private ClaimsPrincipal GenerateClaims()
        {
            var claims = new ClaimsIdentity(AlatTulisAuthenticationScheme.Cookie);

            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, Form.Username));
            claims.AddClaim(new Claim(ClaimTypes.Role, RoleDb));

            return new ClaimsPrincipal(claims);
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("~/");
            }
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var loginDb = await this._alatTulisMan.GetLogin(Form.Username);
            if (loginDb == null)
            {
                SuccessMessage = "Invalid username or password.";
                return Page();
            }

            var usernameDb = loginDb.Username;
            var passwordDb = loginDb.PasswordUser;
            RoleDb = loginDb.RoleUser;

            //if (loginDb.IsActive == false)
            //{
            //    SuccessMessage = "Account belum diaktivasi, silahkan cek email.";
            //    return Page();
            //}

            var isUserMatched = Form.Username == usernameDb;
            var isPassMatched = _alatTulisMan.Verify(Form.Password, passwordDb);

            var isCredentialsOk = isUserMatched && isPassMatched;
            if (isCredentialsOk == false)
            {
                SuccessMessage = "Invalid username or password.";
                return Page();
            }

            var claims = GenerateClaims();
            var persistence = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(15),
                IsPersistent = true
            };

            await HttpContext.SignInAsync(AlatTulisAuthenticationScheme.Cookie, claims, persistence);

            if (string.IsNullOrEmpty(returnUrl) == false)
            {
                return LocalRedirect(returnUrl);
            }

            return Redirect("~/");
        }
    }
}
