using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PosKasir.Models;

namespace PosKasir.Pages.Auth
{
    [Authorize(Roles = RoleModel.UserAdmin)]
    public class LogoutModel : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(AlatTulisAuthenticationScheme.Cookie);
            }
            return Redirect("~/Auth/Login");
        }
    }
}
