using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir.Pages.Cart
{
    [Authorize(Roles = RoleModel.UserAdmin)]
    public class InsertModel : PageModel
    {
        private readonly AlatTulisService _alatTulisMan;
        [BindProperty(SupportsGet = true)]
        public ViewModel Form { get; set; }

        [BindProperty(SupportsGet = true)]
        public List<AlatTulisViewModel> List { get; set; }

        [TempData]
        public string SuccessMessage { get; set; }

        [TempData]
        public int SuccessValue { get; set; }

        public string Message { get; set; }

        public InsertModel(AlatTulisService alatTulisService)
        {
            this._alatTulisMan = alatTulisService;
        }
        public async Task<IActionResult> OnGetAsync(string id)
        {
            List = await _alatTulisMan.GetAllFood();
            return Page();

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var findLogin = await this._alatTulisMan.GetLogin(userNameLogin);
            Form.UserId = findLogin.UserId;

            var checkProductExist = await _alatTulisMan.CheckCartChargeExist(Form);

            if (checkProductExist == false)
            {
                SuccessValue = 1;
                SuccessMessage = "Produk telah ada di keranjang belanja";
                return RedirectToPage();
            }

            var stock = await _alatTulisMan.CheckStock(Form);

            if (stock == false)
            {
                SuccessValue = 1;
                SuccessMessage = "Stock anda invalid, pastikan jumlah Quantity tidak kosong dan tidak melebihi jumlah stock ";
                return RedirectToPage();
            }
            else
            {
                await _alatTulisMan.InsertCart(Form);
            }
            SuccessValue = 2;
            SuccessMessage = "Berhasil memasukan barang ke keranjang";
            return Redirect("/Cart/Index");
        }
    }
}
