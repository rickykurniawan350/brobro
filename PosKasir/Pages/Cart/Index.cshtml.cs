using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir.Pages.Cart
{
    [Authorize(Roles = RoleModel.UserAdmin)]
    public class IndexModel : PageModel
    {
        private readonly AlatTulisService _alatTulisMan;
        private readonly IDataProtector _protector;

        public List<CartModel> Carts { get; set; }
        public ViewModel Bill { get; set; }

        [TempData]
        public string SuccessMessage { get; set; }
        public IndexModel(AlatTulisService alatTulisService, IDataProtectionProvider provider)
        {
            this._alatTulisMan = alatTulisService;
            this._protector = provider.CreateProtector("AlatTulisProtector");
        }
        public async Task<IActionResult> OnGetAsync()
        {

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var findLogin = await this._alatTulisMan.GetLogin(userNameLogin);

            await this._alatTulisMan.FetchAllById(findLogin.UserId);
            this.Carts = this._alatTulisMan.Carts;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var findLogin = await this._alatTulisMan.GetLogin(userNameLogin);
            await this._alatTulisMan.FetchAllById(findLogin.UserId);
            this.Carts = this._alatTulisMan.Carts;

            var tanggalSekarang = DateTimeOffset.Now;

            foreach (var item in this.Carts)
            {
                Bill = new ViewModel()
                {
                    UserId = item.UserId,
                    AlatTulisId = item.AlatTulisId,
                    AlatTulisName = item.AlatTulisName,
                    Stock = item.Stock,
                    PurchaseDate = tanggalSekarang
                };

                var result = await this._alatTulisMan.InsertOrder(Bill);
                if (result == false)
                {
                    SuccessMessage = "Failed";
                    return Page();
                }

            };

            await this._alatTulisMan.SaveToDatabase();

            return Page();
        }

        public IActionResult OnPostEdit(int UnProtected)
        {
            var timeLimitProtector = _protector.ToTimeLimitedDataProtector();

            string tempUnProtected = UnProtected.ToString();
            string ProtectedData = timeLimitProtector.Protect(tempUnProtected, lifetime: TimeSpan.FromSeconds(5));

            return Redirect($"~/Cart/Update/{ProtectedData}");
        }

        public IActionResult OnPostDelete(int UnProtected)
        {
            var timeLimit = _protector.ToTimeLimitedDataProtector();

            string tempUnProtected = UnProtected.ToString();
            string ProtectedData = timeLimit.Protect(tempUnProtected, lifetime: TimeSpan.FromSeconds(5));

            return Redirect($"~/Cart/Delete/{ProtectedData}");
        }
    }
}
