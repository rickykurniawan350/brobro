using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir.Pages.Cart
{
    [Authorize(Roles = RoleModel.Admin)]
    public class UpdateModel : PageModel
    {

        private readonly AlatTulisService _alatTulisMan;
        private readonly IDataProtector _protector;
        [BindProperty(SupportsGet = true)]
        public string ProtectedData { get; set; }

        [BindProperty(SupportsGet = true)]
        public ViewModel Form { set; get; }

        public string Message { get; set; }

        public UpdateModel(AlatTulisService alatTulisService, IDataProtectionProvider provider)
        {
            this._alatTulisMan = alatTulisService;
            _protector = provider.CreateProtector("AlatTulisProtector");
        }
        public async Task<IActionResult> OnGetAsync()
        {

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._alatTulisMan.GetLogin(userNameLogin)).UserId;

            var timeLimit = _protector.ToTimeLimitedDataProtector();
            Form.AlatTulisId = Convert.ToInt32(timeLimit.Unprotect(ProtectedData));

            Form = await _alatTulisMan.GetPurchaseByFoodId(Form.AlatTulisId, userIdLogin);

            if (Form == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._alatTulisMan.GetLogin(userNameLogin)).UserId;

            try
            {
                var timeLimit = _protector.ToTimeLimitedDataProtector();
                Form.AlatTulisId = Convert.ToInt32(timeLimit.Unprotect(ProtectedData));
            }
            catch (Exception e)
            {
                return RedirectToPage("/Cart/Index");
            }


            Form = new ViewModel()
            {
                UserId = userIdLogin,
                AlatTulisId = Form.AlatTulisId,
                Stock = Form.Stock
            };
            var stock = await _alatTulisMan.CheckStock(Form);

            if (stock == false)
            {
                Message = "Permintaan melebihi Stock, mohon kurangi jumlah pembelian (Quantity) Anda";
                return RedirectToPage();
            }
            else
            {

                await _alatTulisMan.UpdateCart(Form);
            }

            return RedirectToPage("/Cart/Index");
        }
    }
}
