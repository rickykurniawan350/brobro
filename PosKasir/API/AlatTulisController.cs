﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir.API
{
    [AllowAnonymous]
    [Route("api/v1/transaction")]
    [ApiController]
    public class AlatTulisController : ControllerBase
    {
        private readonly AlatTulisService _alatTulisMan;

        public AlatTulisController(AlatTulisService alatTulisService)
        {
            this._alatTulisMan = alatTulisService;
        }

        [HttpGet("check-exist", Name = "CheckExistUsername")]
        public async Task<ActionResult<bool>> CheckExistUsername(string username)
        {
            var isExist = await this._alatTulisMan.CheckExistUsername(username);

            return Ok(isExist);
        }

        [HttpPost("register", Name = "Register")]
        public async Task<ActionResult<ResponseModel>> InsertUser([FromBody]RegisterFormModel value)
        {
            var isSuccess = await this._alatTulisMan.InsertUser(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed to register new user"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to register new user"
            });
        }
    }
}
