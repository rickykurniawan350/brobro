using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PosKasir.Entities;
using PosKasir.Models;
using PosKasir.Service;

namespace PosKasir
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //untuk cookie
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.Lax;
                options.HttpOnly = HttpOnlyPolicy.Always;
                options.Secure = CookieSecurePolicy.SameAsRequest;
            });

            services.AddAntiforgery(Q =>
            {
                Q.Cookie.IsEssential = true;
                Q.Cookie.HttpOnly = true;
                Q.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                Q.Cookie.SameSite = SameSiteMode.Lax;
                Q.SuppressXFrameOptionsHeader = true;
            });

            services.AddRazorPages();

            services.AddTransient<AlatTulisService>();

            //untuk cache
            services.AddDistributedMemoryCache();

            services.AddDbContextPool<PosKasirDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PosKasirConnString"), strategy =>
                {
                    strategy.EnableRetryOnFailure();
                }
                );
            });

            //untuk authentication
            services.AddAuthentication(AlatTulisAuthenticationScheme.Cookie)
                    .AddCookie(AlatTulisAuthenticationScheme.Cookie, options =>
                    {
                        options.LoginPath = "/Auth/Login";
                        options.LogoutPath = "/Auth/Logout";
                        options.AccessDeniedPath = "/Auth/AccessDenied";

                        options.Cookie.IsEssential = true;
                        options.Cookie.HttpOnly = true;
                        options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                    });

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });

            //swagger
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Version = "v1", Title = "PosKasir API" });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            //cookie
            app.UseCookiePolicy();

            //authentication
            app.UseAuthentication();
            //authorization
            app.UseAuthorization();

            //swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PosKasir API");
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
        }
    }
}
