﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class LoginFormModel
    {
        [Required(ErrorMessage = "UserName must be filled.")]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password must be filled.")]
        [StringLength(255, MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
