﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class RegisterFormModel
    {
        [Required]
        [StringLength(20, MinimumLength = 3)]
        [Display(Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// password untuk register form
        /// </summary>
        [Required]
        [StringLength(255, MinimumLength = 8)]
        [Display(Name = "Password")]
        public string PasswordUser { get; set; }

        /// <summary>
        /// confirm Password untuk dicocokan dengan password
        /// </summary>
        [Compare("PasswordUser")]
        [Required]
        [StringLength(255, MinimumLength = 8)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPasswordUser { get; set; }
    }
}
