﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class CartModel
    {
        public int UserId { get; set; }

        public int AlatTulisId { get; set; }
        public string AlatTulisName { get; set; }

        public int Stock { get; set; }

        public DateTimeOffset PurchaseDate { get; set; }
    }
}
