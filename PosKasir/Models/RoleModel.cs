﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class RoleModel
    {
        public const string User = "User";
        public const string Admin = "Admin";
        public const string UserAdmin = "User, Admin";
    }
}
