﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class AlatTulisViewModel
    {
        public int AlatTulisId { get; set; }
        public string AlatTulisName { get; set; }
        public decimal Price { get; set; }
        public int Stock { get; set; }
    }
}
