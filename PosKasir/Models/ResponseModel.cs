﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PosKasir.Models
{
    public class ResponseModel
    {
        /// <summary>
        /// string untuk return pada API
        /// </summary>
        public string ResponseMessage { get; set; }
    }
}
