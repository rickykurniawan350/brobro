﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using PosKasir.Entities;
using PosKasir.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace PosKasir.Service
{
    public class AlatTulisService
    {
        private readonly PosKasirDbContext _posKasirDbContext;
        private readonly IDistributedCache _cacheMan;
        private readonly string _cacheKey = "AlatTulis";
        public List<CartModel> Carts = new List<CartModel>();
        public List<ViewModel> Lists = new List<ViewModel>();
        public bool IsLoaded = false;

        public AlatTulisService(PosKasirDbContext dbContext, IDistributedCache distributedCache)
        {
            this._posKasirDbContext = dbContext;
            this._cacheMan = distributedCache;
        }
        public async Task<UserModel> GetLogin(string username)
        {
            var getData = await this._posKasirDbContext.TbUser
                .Where(Q => Q.Username == username)
                .Select(Q => new UserModel
                {
                    UserId = Q.UserId,
                    Username = Q.Username,
                    PasswordUser = Q.PasswordUser,
                    RoleUser = Q.RoleUser,
                }).FirstOrDefaultAsync();

            return getData;
        }

        public bool Verify(string notHash, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(notHash, hash);
        }

        public async Task FetchAllById(int userId)
        {
            if (this.IsLoaded)
            {
                return;
            }

            var all = await _cacheMan.GetStringAsync(this._cacheKey);
            this.IsLoaded = true;
            if (all == null)
            {
                return;
            }

            this.Carts = JsonSerializer.Deserialize<List<CartModel>>(all); //1

            //this.Purchases = JsonConvert.DeserializeObject<List<PurchaseCartModel>>(all);

            this.Carts = this.Carts
                .Where(Q => Q.UserId == userId)
                .ToList();
        }
        public async Task<bool> SaveToDatabase()
        {
            await this._posKasirDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> InsertOrder(ViewModel model)
        {
            this._posKasirDbContext.TbPurchase.Add(new TbPurchase
            {
                PurchaseDate = model.PurchaseDate,
                UserId = model.UserId
            });

            var item = await this._posKasirDbContext
                .TbAlatTulis
                .Where(Q => Q.AlatTulisId == model.AlatTulisId)
                .FirstOrDefaultAsync();

            if (item != null)
            {
                item.Stock = item.Stock - model.Stock;
            }

            await _posKasirDbContext.SaveChangesAsync();
            return true;
            //this._db.TbDetailTransaction.Add(new TbDetailTransaction
            //{
            //    UserId = model.UserId,
            //    FoodId = model.FoodId,
            //    Quantity = model.Stock
            //});
        }

        public async Task<List<AlatTulisViewModel>> GetAllFood()
        {
            var getCharge = await _posKasirDbContext.TbAlatTulis
                .Select(Q => new AlatTulisViewModel
                {
                    AlatTulisId = Q.AlatTulisId,
                    AlatTulisName = Q.AlatTulisName,
                    Price = Q.Price,
                    Stock = Q.Stock
                }).ToListAsync();

            return getCharge;
        }

        public async Task<AlatTulisViewModel> GetAlatTulisDataById(int alatTulisId)
        {
            var getAlatTulis = await _posKasirDbContext.TbAlatTulis
                .Where(Q => Q.AlatTulisId == alatTulisId)
                .Select(Q => new AlatTulisViewModel
                {
                    AlatTulisName = Q.AlatTulisName,
                    Price = Q.Price,
                    Stock = Q.Stock
                })
                .FirstOrDefaultAsync();

            return getAlatTulis;
        }

        public async Task<bool> CheckStock(ViewModel model)
        {
            var getProduct = await GetAlatTulisDataById(model.AlatTulisId);

            if (model.Stock > getProduct.Stock)
            {
                return false;
            }
            else if (model.Stock == 0)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CheckCartChargeExist(ViewModel model)
        {
            await FetchAllById(model.UserId);

            var productExist = Carts.Where(Q => Q.AlatTulisId == model.AlatTulisId).FirstOrDefault();

            if (productExist != null)
            {
                return false;
            }

            return true;
        }

        private async Task SaveToCache(int userId)
        {
            await FetchAllById(userId);
            //var serialized = JsonConvert.SerializeObject(this.Purchases);
            var serialized = JsonSerializer.Serialize(this.Carts); //{ProductId:1}
            await _cacheMan.SetStringAsync(this._cacheKey, serialized);
        }

        public async Task InsertCart(ViewModel model)
        {
            var getProduct = await GetAlatTulisDataById(model.AlatTulisId);

            await FetchAllById(model.UserId);

            this.Carts.Add(new CartModel
            {
                UserId = model.UserId,
                AlatTulisId = model.AlatTulisId,
                AlatTulisName = getProduct.AlatTulisName,
                PurchaseDate = DateTimeOffset.UtcNow,
                Stock = model.Stock
            });

            await SaveToCache(model.AlatTulisId);
        }

        public async Task<ViewModel> GetPurchaseByFoodId(int alatTulisId, int userId)
        {
            await FetchAllById(userId);

            var findProduct = Carts
                .Where(Q => Q.AlatTulisId == alatTulisId)
                .FirstOrDefault();

            if (findProduct == null)
            {
                return new ViewModel();
            }

            var result = new ViewModel()
            {
                UserId = findProduct.UserId,
                AlatTulisId = findProduct.AlatTulisId,
                AlatTulisName = findProduct.AlatTulisName,
                Stock = findProduct.Stock
            };

            return result;
        }

        public async Task UpdateCart(ViewModel model)
        {
            await FetchAllById(model.UserId);
            var findProduct = Carts
                .Where(Q => Q.AlatTulisId == model.AlatTulisId)
                .FirstOrDefault();
            findProduct.Stock = model.Stock;
            await SaveToCache(model.UserId);
        }

        public async Task DeleteCart(int alatTulisId, int userId)
        {
            await FetchAllById(userId);

            var deletePurchase = Carts
                .Where(Q => Q.AlatTulisId == alatTulisId)
                .FirstOrDefault();

            this.Carts.Remove(deletePurchase);

            await SaveToCache(userId);
        }

        public async Task<bool> CheckExistUsername(string username)
        {
            var checkExist = await _posKasirDbContext.TbUser
                            .Where(Q => Q.Username == username)
                            .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> InsertUser(RegisterFormModel model)
        {
            this._posKasirDbContext
                .TbUser
                .Add(new TbUser
                {
                    Username = model.Username,
                    PasswordUser = Hash(model.PasswordUser),
                    RoleUser = RoleModel.User
                });

            await _posKasirDbContext.SaveChangesAsync();
            //await SendMail(PurposeEnum.ActivateUser, model.Email, activationLink);
            return true;
        }

        public string Hash(string password)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, 12);
        }
    }
}
