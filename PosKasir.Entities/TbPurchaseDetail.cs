﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PosKasir.Entities
{
    public partial class TbPurchaseDetail
    {
        [Key]
        public int PurchaseDetailId { get; set; }
        public int PurchaseId { get; set; }
        public int UserId { get; set; }
        public int AlatTulisId { get; set; }
        public int Quantity { get; set; }

        [ForeignKey(nameof(AlatTulisId))]
        [InverseProperty(nameof(TbAlatTulis.TbPurchaseDetail))]
        public virtual TbAlatTulis AlatTulis { get; set; }
        [ForeignKey(nameof(PurchaseId))]
        [InverseProperty(nameof(TbPurchase.TbPurchaseDetail))]
        public virtual TbPurchase Purchase { get; set; }
        [ForeignKey(nameof(UserId))]
        [InverseProperty(nameof(TbUser.TbPurchaseDetail))]
        public virtual TbUser User { get; set; }
    }
}
