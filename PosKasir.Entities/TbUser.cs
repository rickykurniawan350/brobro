﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PosKasir.Entities
{
    public partial class TbUser
    {
        public TbUser()
        {
            TbPurchase = new HashSet<TbPurchase>();
            TbPurchaseDetail = new HashSet<TbPurchaseDetail>();
        }

        [Key]
        public int UserId { get; set; }
        [Required]
        [StringLength(25)]
        public string Username { get; set; }
        [Required]
        [StringLength(255)]
        public string PasswordUser { get; set; }
        [Required]
        [StringLength(25)]
        public string RoleUser { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<TbPurchaseDetail> TbPurchaseDetail { get; set; }
    }
}
