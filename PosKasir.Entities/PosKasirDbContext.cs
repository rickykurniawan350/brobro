﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PosKasir.Entities
{
    public partial class PosKasirDbContext : DbContext
    {
        //public PosKasirDbContext()
        //{
        //}

        public PosKasirDbContext(DbContextOptions<PosKasirDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbAlatTulis> TbAlatTulis { get; set; }
        public virtual DbSet<TbPurchase> TbPurchase { get; set; }
        public virtual DbSet<TbPurchaseDetail> TbPurchaseDetail { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=DbKasir;Username=postgres;Password=12345678;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbAlatTulis>(entity =>
            {
                entity.HasKey(e => e.AlatTulisId)
                    .HasName("PK_Materi");

                entity.Property(e => e.AlatTulisId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbPurchase>(entity =>
            {
                entity.HasKey(e => e.PurchaseId)
                    .HasName("PK_Purchase");

                entity.Property(e => e.PurchaseId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_User");
            });

            modelBuilder.Entity<TbPurchaseDetail>(entity =>
            {
                entity.HasKey(e => e.PurchaseDetailId)
                    .HasName("PK_PurchaseDetail");

                entity.Property(e => e.PurchaseDetailId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.AlatTulis)
                    .WithMany(p => p.TbPurchaseDetail)
                    .HasForeignKey(d => d.AlatTulisId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseDetail_AlatTulis");

                entity.HasOne(d => d.Purchase)
                    .WithMany(p => p.TbPurchaseDetail)
                    .HasForeignKey(d => d.PurchaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseDetail_Purchase");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbPurchaseDetail)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseDetail_User");
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.Username)
                    .HasName("TbUser_Username_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
