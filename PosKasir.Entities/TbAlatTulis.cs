﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PosKasir.Entities
{
    public partial class TbAlatTulis
    {
        public TbAlatTulis()
        {
            TbPurchaseDetail = new HashSet<TbPurchaseDetail>();
        }

        [Key]
        public int AlatTulisId { get; set; }
        [Required]
        [StringLength(255)]
        public string AlatTulisName { get; set; }
        [Column(TypeName = "numeric")]
        public decimal Price { get; set; }
        public int Stock { get; set; }

        [InverseProperty("AlatTulis")]
        public virtual ICollection<TbPurchaseDetail> TbPurchaseDetail { get; set; }
    }
}
